import './App.css';
import Price from './pages/price/price';
import Terms from './pages/terms/terms';
import WelcomePage from './pages/welcome/welcome';
import ContactUs from './pages/contact_us/contact_us';
import { BrowserRouter , Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<WelcomePage/>}/>
        <Route path="/price" element={<Price/>} />
        <Route path="/terms" element={<Terms/>} />
        <Route path="/contact_us" element={<ContactUs/>} />
      </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
