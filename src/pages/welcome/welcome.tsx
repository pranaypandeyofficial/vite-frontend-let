import React from "react";

const WelcomePage: React.FC = ()=>{
    return (<>
        Hello to {import.meta.env.VITE_SOME_KEY}!
    </>);
};

export default WelcomePage;