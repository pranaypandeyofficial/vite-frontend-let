import React, { useState, useEffect } from "react";
import DashNav from "../../components/dashNav/dashNav";
import "./price.css";
import DashSidebar from "../../components/dashSidebar/dashSidebar";
import searchIcon from "./../../assets/search.png";
import newIcon from "./../../assets/price-list/new.png";
import printIcon from "./../../assets/price-list/print.png";
import advancedIcon from "./../../assets/price-list/advanced_mode_toggle_on.png";
import { FaArrowDown } from "react-icons/fa";
import { HiDotsHorizontal } from "react-icons/hi";
import axios from "axios";

interface Article {
  key : string;
  article_no: string;
  product: string;
  in_price: string;
  price: string;
  unit: string;
  in_stock: string;
  description: string;
}

const breakText = (text: string, minCharCount: number): string => {
  const words = text.split(' ');

  const lines: string[] = [];
  let currentLine = '';

  for (const word of words) {
    if (currentLine.length + word.length + 1 <= minCharCount) {
      currentLine += (currentLine.length > 0 ? ' ' : '') + word;
    } else {
      lines.push(currentLine);
      currentLine = word;
    }
  }

  lines.push(currentLine);
  return lines.join('\n');
};

const Price: React.FC = () => {
  const [isDesktop, setIsDesktop] = useState<boolean>((window.innerWidth > 1000)?true:false);
  const [isTablet, setIsTablet] = useState<boolean>((window.innerWidth > 768)?true:false);

  const handleTextDisplayResize = () => {
    setIsDesktop(window.innerWidth > 1000);
    setIsTablet(window.innerWidth > 768);
  };

  useEffect(() => {
    window.addEventListener("resize", handleTextDisplayResize);
    return () => {
      window.removeEventListener("resize", handleTextDisplayResize);
    };
  }, [window.innerWidth  ]);

  const [articleList, setArticleList] = useState<Article[]>([]);
  useEffect(() => {
    axios.get(`${import.meta.env.VITE_APP_URL_PREFIX}/pricelist`).then((response) => {
      let data = response.data;
      let articles = [] as Article[];

      data.forEach(
        (element: {
          articleno: string;
          product: string;
          inPrice: string;
          price: string;
          unit: string;
          inStock: string;
          description: string;
        }) => {
          articles.push({
            key: element.articleno,
            article_no: element.articleno,
            product: element.product,
            in_price: element.inPrice,
            price: element.price,
            unit: element.unit,
            in_stock: element.inStock,
            description: element.description,
          });
        }
      );

      setArticleList(articles);
    }).catch((reason:any)=>{
      console.log('Error in fetching data from server', reason)
    });
  }, []);

  return (
    <div className="price">
      <DashNav />
      <div className="price-content">
        <div className="price-dashSidebar">
          <DashSidebar page={"Price List"} />
        </div>
        <div className="price-main">
          <div className="price--header">
            <div className="price--searchboxes">
              <div className="price--searchbox">
                <input
                  type="text"
                  placeholder="Search Article No."
                  className="price--searchinput"
                />
                <img
                  className="price--searchicon"
                  src={searchIcon}
                  height={"20px"}
                  alt="search-icon"
                />
              </div>
              <div className="price--searchbox">
                <input
                  type="text"
                  placeholder="Search Product"
                  className="price--searchinput"
                />
                <img
                  className="price--searchicon"
                  src={searchIcon}
                  height={"20px"}
                  alt="search-icon"
                />
              </div>
            </div>
            <div className="price--utils">
              <div className="price--util">
                {isDesktop && "New Product "}{" "}
                <img className="price--utils--icons" src={newIcon} alt="" />
              </div>
              <div className="price--util">
                {isDesktop && "Print List "}{" "}
                <img className="price--utils--icons" src={printIcon} alt="" />
              </div>
              <div className="price--util">
                {isDesktop && "Advanced Mode "}{" "}
                <img
                  className="price--utils--icons"
                  src={advancedIcon}
                  alt=""
                />
              </div>
            </div>
          </div>
          <div className="price--articles">
            {isTablet && (
              <div className="price--article--no price--article--column">
                <div className="price--article--header">
                  Article No. {isDesktop && <FaArrowDown color="lightblue" />}
                </div>
                <div className="price--article--list">
                  {articleList.map((article) => (
                    <div className="price--article--item">
                      {article.article_no}
                    </div>
                  ))}
                </div>
              </div>
            )}
            <div className="price--article--product price--article--column">
              <div className="price--article--header">
                Product / Service
                {isDesktop && <FaArrowDown color="lightgreen" />}
              </div>
              <div className="price--article--list">
                {articleList.map((article) => (
                  <div className="price--article--item">{ breakText(article.product, 15)}</div>
                ))}
              </div>
            </div>
            {isDesktop && (
              <div className="price--article--inPrice price--article--column">
                <div className="price--article--header">In Price</div>
                <div className="price--article--list">
                  {articleList.map((article) => (
                    <div className="price--article--item">
                      {article.in_price}
                    </div>
                  ))}
                </div>
              </div>
            )}
            <div className="price--article--price price--article--column">
              <div className="price--article--header">Price</div>
              <div className="price--article--list">
                {articleList.map((article) => (
                  <div className="price--article--item">{article.price}</div>
                ))}
              </div>
            </div>
            {isTablet && (
              <div className="price--article--unit price--article--column">
                <div className="price--article--header">Unit</div>
                <div className="price--article--list">
                  {articleList.map((article) => (
                    <div className="price--article--item">{article.unit}</div>
                  ))}
                </div>
              </div>
            )}
            {isTablet && (
              <div className="price--article--inStock price--article--column">
                <div className="price--article--header">In Stock</div>
                <div className="price--article--list">
                  {articleList.map((article) => (
                    <div className="price--article--item">
                      {article.in_stock}
                    </div>
                  ))}
                </div>
              </div>
            )}
            {isDesktop && (
              <div className="price--article--description price--article--column">
                <div className="price--article--header">Description</div>
                <div className="price--article--list">
                  {articleList.map((article) => (
                    <div className="price--article--item price-article-description">
                      {breakText(article.description, 15)}
                    </div>
                  ))}
                </div>
              </div>
            )}
            <div className="price--article--knowMore price--article--column">
              <div className="price--article--header"> </div>
              <div className="price--article--list">
                {articleList.map(() => (
                  <div>
                    <HiDotsHorizontal
                      className="price--article--more"
                      color="blue"
                      width={"40px"}
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Price;
