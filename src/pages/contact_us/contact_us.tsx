import React, {useEffect, useState} from "react";
import './contact_us.css';
import OtherNav from "../../components/otherNavbar/otherNav";
import facebookIcon from "../../assets/facebook.png";
import instagramIcon from "../../assets/instagram.png";
import linkedInIcon from "../../assets/linkedin.png";
import pinterestIcon from "../../assets/pintrest.png";
import Footer from "../../components/footer/footer";
import Background from "../../components/background/background";
import { TextInterface } from "../../components/utils";
import axios from "axios";


const ContactUs : React.FC = () => {

    const [headerText, setHeaderText] = useState<string>("");
    const [subheaderText, setSubHeaderText] = useState<string>("");
    const [buttonText, setButtonText] = useState<string>("");
    const [text, setText] = useState<TextInterface[]>([]);

    useEffect(()=>{
        axios.get(`${import.meta.env.VITE_APP_URL_PREFIX}/contactpage`).then(
            (response)=>{
                try{
                    setHeaderText(response.data["header"]);
                    setSubHeaderText(response.data["p1"]);
                    setButtonText(response.data["buttonText"]);
                    let textArray: TextInterface[] = [];
                    for (let field in response.data){
                        if (field.includes("p")){
                            textArray.push({text: response.data[field]});
                        }
                    }
                    setText(textArray);
                }
                catch(err){
                    console.error("Error while setting text for contact page", err);
                }
            }
        ).catch(
            (err)=>{
                console.error("Error while getting text for contact page", err);
            }
        );
    }, []);

    return (
        <>
        <div>
            <Background/>
            <OtherNav/>
            <section className="contact_us">
            <div className="contact--text">
                <p className="red--text">
                {headerText}
                </p>

                <p className="normal--text">
                    {subheaderText}
                </p>

                <p className="raised--button">
                    <a className="mail--id" href="mailto:info@LettFaktura.no?subject=Kontakt">
                    {buttonText}
                    </a>
                </p>

                {text.map((item, index)=>{
                    return(
                        <p key={index} className="normal--text">{item.text}</p>
                    )
                })}

                {/* <p className="normal--text">
                If you prefer post - send to:
                </p>

                <p className="normal--text">
                LettFaktura<br/>
                Pb. 188<br/>
                3166 Tolvsrød
                </p>

                <p className="normal--text">
                Telephone 31 10 30 00<br/>
                Fax 31 10 30 01
                </p> */}
            </div>
        </section>
        <div className="socialMedia--container">
        <a  href="https://www.facebook.com/Lettfaktura/" className="w-inline-block"><img src={facebookIcon} alt="" /></a>
      <a  href="https://www.instagram.com/lettfaktura" className="w-inline-block"><img src={instagramIcon} alt=""/></a>
      <a href="https://www.linkedin.com/company/lettfaktura"  className="w-inline-block"><img src={linkedInIcon} alt=""/></a>
      <a href="https://www.pinterest.com/lettfaktura"  className="w-inline-block"><img src={pinterestIcon} alt=""/></a>
        </div>
        </div>
        <Footer/>
        </>
    );
};

export default ContactUs;