import axios from 'axios';
import { useEffect, useRef } from 'react';

export interface TextInterface{
    text: string;
}

export interface Language {
  name: string;
  flag: string;
}

const fetchLanguageData = async (): Promise<Language[]> => {
  try {
    const response = await axios.get(`${import.meta.env.VITE_APP_URL_PREFIX}/languages`);
    const data = response.data;

    if (data) {
      let languageData: Language[] = [];
      for ( let i = 0; i < data.length; i++){
        languageData.push({name: data[i].language, flag: data[i].icon});
      }
      return languageData;
    } else {
      console.error('Invalid data received from the server.');
      return [];
    }
  } catch (error) {
      console.error('Unexpected error:', error);

    return [];
  }
};

export {fetchLanguageData};

const getImages = async ():Promise<Record<string, string>> => {
    try{
    const response = await axios.get(`${import.meta.env.VITE_APP_URL_PREFIX}/images`);
    const data = await response.data;
    
        if (data) {
        return data;
      } else {
        console.error('Invalid data received from the server.');
        return {};
      }
    }
     catch (error) {
        console.error('Unexpected error:', error);
  
      return {};
    }
};

export {getImages};

const useClickOutside = (callback: () => void, className: string) => {
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      // if (ref.current && !ref.current.contains(event.target as Node)) {
      //   callback();
      // }
      const target = event.target as HTMLElement;
        if (!target.closest(className)) {
         callback();
        }
    };

    document.addEventListener('click', handleClickOutside);

    return () => {
      document.removeEventListener('click', handleClickOutside);
    };
  }, [callback, className]);

  return ref;
};

export {useClickOutside};