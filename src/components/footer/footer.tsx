import { Link } from "react-router-dom";
import './footer.css';

export default function Footer() {

  const linkCollection = [
    {
      label: 'DOWNLOAD',
      link: "/",
      alt: "Home",
      id: "1",
    },
    {
      label: 'ORDER',
      link: "/order",
      alt: "Order",
      id: "2",
    },
    {
      label: 'CONTACT US',
      alt: "Contactus",
      link: "/contact_us",
      id: "3",
    },
  ];

  const footerLinks = linkCollection.map((collection) => (
    <Link to={collection.link || "#"} key={collection.id} style={{ textDecoration: 'none' }} >
        <p className="footer--label">{collection.label}</p>
    </Link>
  ));

  return (
      <footer
        className="footer"
      >
        <div className="footer--links">
          {footerLinks}
        </div>
        <div >
          <p className="footer--text">
          © Lettfaktura, CRO no. 638535, 2023. All rights reserved.
          </p>
        </div>
      </footer>
  );
}
