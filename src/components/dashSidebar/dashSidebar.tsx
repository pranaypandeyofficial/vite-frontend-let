import './dashSidebar.css';
import React, {useState, useEffect} from 'react';


import axios from 'axios';

interface DashSidebarProps {
    page?: string;
}

interface DashSidebarItem{
    text: string;
    icon: string;
    link: string;
}

const DashSidebar: React.FC<DashSidebarProps> = ({ page = "" }) => {

    const [contents, setContent] = useState<DashSidebarItem[]>([]);
    useEffect(()=>{
        axios.get(`${import.meta.env.VITE_APP_URL_PREFIX}/dashboardbar`).then((response)=>{
            const data = response.data;
            try{
                console.log(data);
                let items = [] as DashSidebarItem[];
                for( let i = 0; i < data.length; i++){
                    items.push({text: data[i].text, icon: data[i].url, link: data[i].link});
                }
                setContent(items);
            }
            catch (error){
                console.error('Unexpected error:', error);
            }
        }).catch((error)=>{
            console.error('Unexpected error:', error);
        });
    }, []);

    return (<>
        <div className="dashSidebar">
            <div className="dashSide--MenuText">Menu</div>
            <div className="dashSide--contents">
                {
                contents.map((content, index) => {
                    return (
                        <a className="dashSide--anchor" href={content.link}>
                            <div className="dashSide--content" key={index}>

                                <div className={page===content.text?"dashSide-active":"dashSide-inactive"}></div>
                                <div className="dashSide--contentIcon">
                                    <img src={content.icon} alt=""/>
                                </div>
                                <div className="dashSide--contentText">
                                    {content.text}
                                </div>
                            </div>
                        </a>
                    );
                })
                }
            </div>
        </div>
    </>);
};

export default DashSidebar;