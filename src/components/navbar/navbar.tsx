import { useState, useEffect } from 'react';
import { FaBars } from 'react-icons/fa'; // Import hamburger icon
import './navbar.css';
import { Link } from 'react-router-dom';
import { fetchLanguageData, Language, getImages, useClickOutside } from '../utils';


const Navbar = () => {
  const [navLogo, setNavLogo] = useState<string>('');
  const [languages, setLanguages] = useState<Language[]>([{name: '', flag: ''}]);
  const [currentLanguage, setCurrentLanguage] = useState(languages[0]);
  const [showDropdown, setShowDropdown] = useState(false);
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 1250);
  const [menuDropdown, setMenuDropdown] = useState(false);

  useEffect(() => {

    let isMounted = true;
  fetchLanguageData().then((data) => {
    if (isMounted && data && data.length > 0){
      console.log("LOggin ")
      console.log(data);
      setLanguages(data as Language[]);
    }
    else{
      console.error("Invalid data received from the server.");
    }
  }).catch((err)=>{
    console.error("Error while fetching languages", err);
  });
 
}, []);

useEffect(() => {
  let isMounted = true;

  getImages().then((data) => {
    try{
      if (isMounted && data &&  data['logo']){
        setNavLogo(data['logo'] as string);
      }
    }
    catch(err){
      console.error("Error while fetching logo image", err);
    }

  }).catch((err) => {
    console.error("Error while fetching logo image", err);
  });
}, []);

  useEffect(() => {
    setCurrentLanguage(languages[0]);
  }, [languages]);


  const handleLanguageChange = (selectedLanguage: { name: string; flag: string }) => {
    setCurrentLanguage(selectedLanguage);
    setShowDropdown(false);
  };

  const handleResize = () => {
    setIsMobile(window.innerWidth <= 1250);
  };
  
  const handleMenuDropdown = () => {
    setMenuDropdown(!menuDropdown);
  }

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useClickOutside(() => setShowDropdown(false), '.langDropdown');
  useClickOutside(() => setMenuDropdown(false), '.menuDropdown');


  return (
    <nav className='Navbar'>
      <div className='Navbar--left'>
        <img className='website--logo' src={navLogo} alt='lettfaktura' height={'30px'} />
        {isMobile && (
          <FaBars className='hamburger-icon' size={"25px"} onClick={handleMenuDropdown} />
        )}

        {isMobile && menuDropdown && (
          <div className="menuDropdown">
            <ul className='Navdropdown--ul'>
              <li>Home</li>
              <li>Order</li>
              <li>Our Customers</li>
              <li>About Us</li>
              <a href='/contact_us' className='Navdropdown--link'><li>Contact Us</li></a>
            </ul>
          </div>
        )}
        
      </div>

      <div className='Navbar--right'>
        {/* Conditionally render the menu based on screen width */}
        {!isMobile && (
          <ul className='Nav--ul'>
            <li>Home</li>
            <li>Order</li>
            <li>Our Customers</li>
            <li>About Us</li>
            <li><Link to={"/contact_us"}>Contact Us</Link></li>
          </ul>
        )}

        <div className="langDropdown">
          <div
            className="langDropdown__selected"
            onClick={() => setShowDropdown(!showDropdown)}
          >
            <p>{currentLanguage.name}</p>
            <img
              src={currentLanguage.flag}
              alt={currentLanguage.name}
              height={'20px'}
            />
          </div>

          {showDropdown && currentLanguage.name!=='' && (
            <div className="langDropdown__options">
              {languages.map((language, index) => (
                <div
                  key={index}
                  className="langDropdown__option"
                  onClick={() => handleLanguageChange(language)}
                >
                  <p>{language.name}</p>
                  <img src={language.flag} alt={language.name} height={'20px'} />
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
    </nav>
  );
}

export default Navbar;